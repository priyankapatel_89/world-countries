package com.homeadvisor.worldcountries;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.homeadvisor.worldcountries.R.raw.countries;


public class MainActivity extends AppCompatActivity implements CountryAdapter.CountryAdapterOnClickHandler {

    private CountryAdapter mCountryAdapter;
    protected ArrayList<Model> countriesName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_countries);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mCountryAdapter = new CountryAdapter(this);

        mRecyclerView.setAdapter(mCountryAdapter);
        mCountryAdapter.setCountryData(countriesName);

        new FetchCountriesTask().execute();

    }

    @Override
    public void onClick(Model countryData) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("countryName", countryData.getCountryName());
        intent.putExtra("nativeName", countryData.getNativeName());
        intent.putExtra("capital", countryData.getCapital());
        intent.putExtra("population", countryData.getPopulation());
        intent.putExtra("region", countryData.getRegion());
        intent.putExtra("subregion", countryData.getSubregion());
        intent.putExtra("area", countryData.getArea());
        intent.putExtra("currencies", countryData.getCurrencies());
        intent.putExtra("languages", countryData.getLanguages());
        intent.putExtra("borders", countryData.getBorders());
        startActivity(intent);
    }

    private class FetchCountriesTask extends AsyncTask<String, Void, ArrayList<Model>> {

        @Override
        protected ArrayList<Model> doInBackground(String... strings) {

            try {

                InputStream inputStream = getResources().openRawResource(countries);
                StringBuilder builder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                String finalJson = builder.toString();

                JSONArray parentArray = new JSONArray(finalJson);

                for (int i = 0; i <parentArray.length(); i++) {
                    JSONObject finalObject = parentArray.getJSONObject(i);
                    Model country = new Model();
                    country.setCountryName(finalObject.getString("name"));
                    country.setNativeName(finalObject.getString("nativeName"));
                    country.setCapital(finalObject.getString("capital"));
                    country.setPopulation(finalObject.getInt("population"));
                    country.setRegion(finalObject.getString("region"));
                    country.setSubregion(finalObject.getString("subregion"));
                    country.setArea(finalObject.optInt("area"));

                    JSONArray currencies = finalObject.getJSONArray("currencies");
                    for (int l = 0; l<currencies.length(); l++) {
                        country.setCurrencies(String.valueOf(currencies)
                                .replace("]", "").replace("[", "").replace("\"", "").replace(",", ", "));
                    }
                    JSONArray languages = finalObject.getJSONArray("languages");
                    for (int k = 0; k<languages.length(); k++) {
                        country.setLanguages(String.valueOf(languages)
                                .replace("]", "").replace("[", "").replace("\"", "").replace(",", ", "));
                    }
                    JSONArray borders = finalObject.getJSONArray("borders");
                    for (int j = 0; j<borders.length(); j++) {
                        country.setBorders(String.valueOf(borders)
                                .replace("]", "").replace("[", "").replace("\"", ""). replace(",", ", "));
                    }

                    countriesName.add(country);
                }
                return countriesName;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Model> results) {
            super.onPostExecute(results);
            if (countriesName != null) {
                mCountryAdapter.setCountryData(countriesName);
            }
        }
    }
}

