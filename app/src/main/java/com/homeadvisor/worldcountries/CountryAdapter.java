package com.homeadvisor.worldcountries;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder>{

    private ArrayList<Model> countriesList = new ArrayList<>();
    private final CountryAdapterOnClickHandler mClickHandler;

    public interface CountryAdapterOnClickHandler{
        void onClick(Model countryData);
    }

    public CountryAdapter(CountryAdapterOnClickHandler clickHandler) {
        mClickHandler = clickHandler;
    }

    public class CountryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mCountryNameTextView;

        public CountryViewHolder(View view){
            super(view);

            mCountryNameTextView = (TextView) view.findViewById(R.id.tv_country_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Model countryData = countriesList.get(getAdapterPosition());
            mClickHandler.onClick(countryData);
        }
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_list_item, parent, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        Model countries = countriesList.get(position);
        holder.mCountryNameTextView.setText(countries.getCountryName());
    }

    @Override
    public int getItemCount() {
        if (countriesList == null) return 0;
        return countriesList.size();
    }

    public void setCountryData(ArrayList<Model> mCountriesList) {
        countriesList = mCountriesList;
        notifyDataSetChanged();
    }

}
