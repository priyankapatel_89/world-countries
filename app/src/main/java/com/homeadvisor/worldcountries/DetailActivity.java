package com.homeadvisor.worldcountries;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Locale;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView mName = (TextView) findViewById(R.id.tv_native_name);
        TextView mCapital = (TextView) findViewById(R.id.tv_capital);
        TextView mPopulation = (TextView) findViewById(R.id.tv_population);
        TextView mRegion = (TextView) findViewById(R.id.tv_region);
        TextView mSubregion = (TextView) findViewById(R.id.tv_subregion);
        TextView mArea = (TextView) findViewById(R.id.tv_area);
        TextView mCurrencies = (TextView) findViewById(R.id.tv_currencies);
        TextView mLanguages = (TextView) findViewById(R.id.tv_languages);
        TextView mBorders = (TextView) findViewById(R.id.tv_borders);

        Intent receivingIntent = getIntent();

        if (receivingIntent != null) {
            if (receivingIntent.hasExtra("countryName")){
                String details = receivingIntent.getStringExtra("countryName");
                this.setTitle(details);
            }
            if (receivingIntent.hasExtra("nativeName")) {
                String details = receivingIntent.getStringExtra("nativeName");
                mName.setText(details);
            }
            if (receivingIntent.hasExtra("capital")) {
                String details = receivingIntent.getStringExtra("capital");
                mCapital.setText(details);
            }
            if (receivingIntent.hasExtra("population")) {
                int details = receivingIntent.getIntExtra("population", 0);
                mPopulation.setText(String.format(Locale.US, "%,d", details));
            }
            if (receivingIntent.hasExtra("region")) {
                String details = receivingIntent.getStringExtra("region");
                mRegion.setText(details);
            }
            if (receivingIntent.hasExtra("subregion")) {
                String details = receivingIntent.getStringExtra("subregion");
                mSubregion.setText(details);
            }
            if (receivingIntent.hasExtra("area")) {
                int details = receivingIntent.getIntExtra("area", 0);
                mArea.setText(String.format("%s%s", details, getString(R.string.km)));

            }
            if (receivingIntent.hasExtra("currencies")) {
                String details = receivingIntent.getStringExtra("currencies");
                mCurrencies.setText(details);
            }
            if (receivingIntent.hasExtra("languages")) {
                String details = receivingIntent.getStringExtra("languages");
                mLanguages.setText(details);
            }
            if (receivingIntent.hasExtra("borders")) {
                String details = receivingIntent.getStringExtra("borders");
                mBorders.setText(details);
            }

        }
    }
}